package com.JeYo.Georges;

public class Pingouin{
  private int id;
  private int lvl;
  private int str;
  private int intel;
  private int agi;
  private int hp;
  private int hpmax;
  private int xp;
  private int xpmax;
  private int gold;
  private Arme[] stuff;

  public Pingouin(){
    id=1;
    lvl = 1; str = 5; intel = 5; agi = 5;
    hp = 20; hpmax = 20; xp = 0; xpmax = 10; gold = 0;
    stuff = new Arme[3];
    stuff[0] = new Arme("Canne 1","La première canne","Canne",3,7,5,2);
    stuff[1] = new Arme("Chapeau 1","Le premier chapeau","Chapeau",5,5,2,2);
    stuff[2] = new Arme("Cape 1","La première cape","Cape",2,4,10,5);
  }

  public int CalcDmgChap() {
    return stuff[1].calDmg(this);
  }

  public int CalcDmgCanne() {
    return stuff[0].calDmg(this);
  }

  public int CalcDmgCape() {
    return stuff[2].calDmg(this);
  }

  public void attack(Monstre m) {
    m.dmgs(CalcDmgCanne());
    if(m.isAlive()==false){
      addXp(m.getXp());
      addPo(m.getGold());
    }
  }

  public void subit(int dmgs) {
    hp -= dmgs;
  }

  public void heal() {
    hp = hpmax;
  }

  public void lvlup(){
    xp = 0;
    xpmax /= 2;
    xpmax *= xpmax;
  }

  public void addXp(int x){
    if (xp + x >= xpmax) {
      int y = xp + x - xpmax;
      lvlup();
      addXp(y);
    }
    else {
      xp += x;
    }
  }

  public void addPo(int p) {
    gold += p;
  }

///////////////////////////////// GETTERS ET SETTERS
  public int getId() { return id; }
  public void setId(int i) { id = i; }
  public int getLvl() { return lvl; }
  public void setLvl(int i) { lvl = i; }
  public int getStr() { return str; }
  public void setStr(int i) { str = i; }
  public int getIntel() { return intel; }
  public void setIntel(int i) { intel = i; }
  public int getAgi() { return agi; }
  public void setAgi(int i) { agi = i; }
  public int getHp() { return hp; }
  public void setHp(int i) { hp = i; }
  public int getHpMax() { return hpmax; }
  public void setHpMax(int i) { hpmax = i;  }
  public int getXp() { return xp; }
  public void setXp(int i) { xp = i; }
  public int getXpMax() { return xpmax; }
  public void setXpMax(int i) { xpmax = i; }
  public int getGold() { return gold; }
  public void setGold(int i) { gold = i; }
}
