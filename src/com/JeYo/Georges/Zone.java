package com.JeYo.Georges;

public class Zone{
  private int id;
  private String nom;
  private int lvl;
  private Monstre[] mob;

  public Zone(String n, int lv) {
    nom = n;
    lvl = lv;
    mob = new Monstre[10];
    for (int i = 0; i < 5; ++i){
      int a= (int)(Math.random()*2+lvl-2);
      if (a < 1){
        a = 1;
      }
      mob[i] = new Monstre(a,this);
    }
    for(int i=5;i<9;++i){
      int a=(int)(Math.random()*2+lvl-2);
      if(a<1){
        a=1;
      }
        mob[i]=new Monstre(a,this);
    }
    mob[9]=new Monstre(lvl+3,this);
  }
///////////////////////////////// GETTERS ET SETTERS
  public int getId() { return id; }
  public void setId(int i) { id = i; }
  public String getNom(){ return nom; }
  public void setNom(String n){ nom = n; }
  public int getLvl() { return lvl; }
  public void setLvl(int i) { lvl = i; }
}
