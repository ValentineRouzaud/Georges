package com.JeYo.Georges;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class Georges extends Activity {

    /**
     * Called when the activity is first created.
     */
    static GeorgesBDD geoBDD;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        geoBDD = new GeorgesBDD(this);

        Pingouin geo = new Pingouin();
        Monstre mon = new Monstre(2);
        geoBDD.open();

        geoBDD.insertMonstre(mon);
        Monstre monBDD = geoBDD.getMonstreWithId(mon.getId());
        try{geoBDD.getPingouinWithId(1).getLvl();}
        catch(Exception e) {
            geoBDD.insertPingouin(geo);
            geoBDD.insertMonstre(mon);
        }


//        //Pour vérifier que l'on a bien créé notre livre dans la BDD
//        //on extrait le livre de la BDD grâce au titre du livre que l'on a créé précédemment
//        Pingouin geoFromBDD = geoBDD.getPingouinWithId(geo.getId());
//        //Si un livre est retourné (donc si le livre à bien été ajouté à la BDD)
//        if(geoFromBDD != null){
//            Toast.makeText(this, geoFromBDD.toString(), Toast.LENGTH_LONG).show();
//            geoBDD.updatePingouin(geoFromBDD.getId(), geoFromBDD);
//        }
//
//        geoFromBDD = geoBDD.getPingouinWithId(geo.getId());
//        if(geoFromBDD == null){
//            Toast.makeText(this, "Pingouin n'existe pas dans la BD", Toast.LENGTH_LONG).show();
//        }
//        else{
//            Toast.makeText(this, "Pingouin est bien dans la BD", Toast.LENGTH_LONG).show();
//        }
        geoBDD.close();
    }

    public void changerEcranStats(View view) {
      Intent intent = new Intent(this, Stats.class);
      startActivity(intent);
    }


    public void soigner(View view) {
        geoBDD.open();
        Pingouin g = geoBDD.getPingouinWithId(1);
        int max = g.getHpMax();
        g.setHp(max);
        geoBDD.removePingouinWithId(1);
        geoBDD.insertPingouin(g);
        geoBDD.close();
        AlertDialog alertDialog = new AlertDialog.Builder(Georges.this).create();
        alertDialog.setTitle("Georges at the nursery");
        alertDialog.setMessage("Georges gets all his life back.");
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    public void creveGeorgesPlizAled(View view) {

        Intent intent = new Intent(this, Travel.class);
        startActivity(intent);
    }

}
