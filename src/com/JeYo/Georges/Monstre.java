package com.JeYo.Georges;

public class Monstre {
  private int id;
  private int idzone;
  private int lvl;
  private int hp;
  private int dmgMin;
  private int dmgMax;
  private int xp; //How much xp you'll earn
  private int gold; //How much gold you'll earn

  static int sid=1;

  public Monstre(){}
  public Monstre(int lv){
    lvl = lv;
    dmgMin = lvl-(int)(Math.random()*lvl);
    if (dmgMin < 0) {
      dmgMin = 0;
    }
    dmgMax = lvl+(int)(Math.random()*lvl);
    gold = lvl;
    xp = lvl;
    hp = lvl*5+(int)(Math.random()*lvl);
    id=sid;
    sid+=1;
    idzone=1;
  }

  public Monstre(int lv, Zone z){
    lvl = lv;
    dmgMin = lvl-(int)(Math.random()*lvl);
    if (dmgMin < 0) {
      dmgMin = 0;
    }
    dmgMax = lvl+(int)(Math.random()*lvl);
    gold = lvl;
    xp = lvl;
    hp = lvl*5+(int)(Math.random()*lvl);

    idzone=z.getId();
  }

  public void dmgs(int dmg){
    hp -= dmg;
  }

  public int attaque(){
    return (int)(Math.random()*(dmgMax-dmgMin))+dmgMin;
  }

  public boolean isAlive(){
    return hp > 0;
  }

  public int getId() { return id; }
  public void setId(int i) { id = i; }
  public int getIdZone() { return idzone; }
  public void setIdZone(int i) { idzone = i; }
  public int getLvl() { return lvl; }
  public void setLvl(int i) { lvl = i; }
  public int getHp() { return hp; }
  public void setHp(int i) { hp = i; }
  public int getDmgMin() { return dmgMin; }
  public void setDmgMin(int i) { dmgMin = i; }
  public int getDmgMax() { return dmgMax; }
  public void setDmgMax(int i) { dmgMax = i; }
  public int getXp() { return xp; }
  public void setXp(int i) { xp = i; }
  public int getGold() { return gold; }
  public void setGold(int i) { gold = i; }
}
