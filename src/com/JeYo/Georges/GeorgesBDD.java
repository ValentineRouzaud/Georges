package com.JeYo.Georges;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class GeorgesBDD {

	private static final int VERSION_BDD = 1;
	private static final String NOM_BDD = "georges.db";

	private static final String TABLE_PINGOUIN = "pingouin";
	private static final String COL_IDG = "ID";
	private static final int NUM_COL_IDG = 0;
	private static final String COL_LVLG = "Lvl";
	private static final int NUM_COL_LVLG = 1;
	private static final String COL_STR = "Str";
	private static final int NUM_COL_STR = 2;
  private static final String COL_INTEL = "Intel";
  private static final int NUM_COL_INTEL = 3;
  private static final String COL_AGI = "Agi";
  private static final int NUM_COL_AGI = 4;
  private static final String COL_HPG = "HP";
  private static final int NUM_COL_HPG = 5;
  private static final String COL_HPMAX = "HPMax";
  private static final int NUM_COL_HPMAX = 6;
  private static final String COL_XPG = "XP";
  private static final int NUM_COL_XPG = 7;
  private static final String COL_XPMAX = "XPMax";
  private static final int NUM_COL_XPMAX = 8;
  private static final String COL_GOLDG = "Gold";
  private static final int NUM_COL_GOLDG = 9;

  private static final String TABLE_ARMES = "armes";
  private static final String COL_IDA = "ID";
  private static final int NUM_COL_IDA = 0;
  private static final String COL_NOM = "Nom";
  private static final int NUM_COL_NOM = 1;
  private static final String COL_DESC = "Description";
  private static final int NUM_COL_DESC = 2;
  private static final String COL_TYPE = "Type";
  private static final int NUM_COL_TYPE = 3;
  private static final String COL_DMGMINA = "DmgMin";
  private static final int NUM_COL_DMGMINA = 4;
  private static final String COL_DMGMAXA = "DmgMax";
  private static final int NUM_COL_DMGMAXA = 5;
  private static final String COL_PROBCRIT = "ProbCrit";
  private static final int NUM_COL_PROBCRIT = 6;
  private static final String COL_AUGCRIT = "AugCrit";
  private static final int NUM_COL_AUGCRIT = 7;

  private static final String TABLE_MONSTRES = "monstres";
  private static final String COL_IDM = "ID";
  private static final int NUM_COL_IDM = 0;
  private static final String COL_IDZONE = "IDZ";
  private static final int NUM_COL_IDZONE = 1;
  private static final String COL_LVLM = "Lvl";
  private static final int NUM_COL_LVLM = 2;
  private static final String COL_HPM = "HP";
  private static final int NUM_COL_HPM = 3;
  private static final String COL_DMGMINM = "DmgMin";
  private static final int NUM_COL_DMGMINM = 4;
  private static final String COL_DMGMAXM = "DmgMax";
  private static final int NUM_COL_DMGMAXM = 5;
  private static final String COL_XPM = "XP";
  private static final int NUM_COL_XPM = 6;
  private static final String COL_GOLDM = "Gold";
  private static final int NUM_COL_GOLDM = 7;

  private static final String TABLE_ZONES = "zones";
	private static final String COL_IDZ = "ID";
  private static final int NUM_COL_IDZ = 0;
	private static final String COL_LVLZ = "Lvl";
  private static final int NUM_COL_LVLZ = 1;

	private SQLiteDatabase bdd;

	private MaBaseSQLite maBaseSQLite;

	public GeorgesBDD(Context context){
		//On créé la BDD et sa table
		maBaseSQLite = new MaBaseSQLite(context, NOM_BDD, null, VERSION_BDD);
	}

	public void open(){
		//on ouvre la BDD en écriture
		bdd = maBaseSQLite.getWritableDatabase();
	}

	public void close(){
		//on ferme l'accès à la BDD
		bdd.close();
	}

	public SQLiteDatabase getBDD(){
		return bdd;
	}

	public long insertPingouin(Pingouin georges){
		//Création d'un ContentValues (fonctionne comme une HashMap)
		ContentValues values = new ContentValues();
		//on lui ajoute une valeur associé à une clé (qui est le nom de la colonne dans laquelle on veut mettre la valeur)
        values.put(COL_IDG, georges.getId());
        values.put(COL_LVLG, georges.getLvl());
				values.put(COL_STR, georges.getStr());
        values.put(COL_INTEL, georges.getIntel());
        values.put(COL_AGI, georges.getAgi());
        values.put(COL_HPG, georges.getHp());
        values.put(COL_HPMAX, georges.getHpMax());
        values.put(COL_XPG, georges.getXp());
        values.put(COL_XPMAX, georges.getXpMax());
        values.put(COL_GOLDG, georges.getGold());
		//on insère l'objet dans la BDD via le ContentValues
		return bdd.insert(TABLE_PINGOUIN, null, values);
	}

	public int updatePingouin(int id, Pingouin georges){
		//La mise à jour d'un livre dans la BDD fonctionne plus ou moins comme une insertion
		//il faut simple préciser quelle livre on doit mettre à jour grâce à l'ID
		ContentValues values = new ContentValues();
		values.put(COL_LVLG, georges.getLvl());
    values.put(COL_STR, georges.getStr());
    values.put(COL_INTEL, georges.getIntel());
    values.put(COL_AGI, georges.getAgi());
    values.put(COL_HPG, georges.getHp());
    values.put(COL_HPMAX, georges.getHpMax());
    values.put(COL_XPG, georges.getXp());
    values.put(COL_XPMAX, georges.getXpMax());
    values.put(COL_GOLDG, georges.getGold());
		return bdd.update(TABLE_PINGOUIN, values, COL_IDG + " = " +id, null);
	}

	public int removePingouinWithId(int id){
		//Suppression d'un livre de la BDD grâce à l'ID
		return bdd.delete(TABLE_PINGOUIN, COL_IDG + " = " +id, null);
	}

	public Pingouin getPingouinWithId(int id){
		//Récupère dans un Cursor les valeur correspondant à un livre contenu dans la BDD (ici on sélectionne le livre grâce à son titre)
		Cursor c = bdd.query(TABLE_PINGOUIN, new String[] {COL_IDG, COL_LVLG, COL_STR, COL_INTEL, COL_AGI, COL_HPG, COL_HPMAX, COL_XPG, COL_XPMAX, COL_GOLDG}, null, null, null, null, null);
		return cursorToPingouin(c);
	}

	//Cette méthode permet de convertir un cursor en un livre
	private Pingouin cursorToPingouin(Cursor c){
		//si aucun élément n'a été retourné dans la requête, on renvoie null
		if (c.getCount() == 0)
			return null;

		//Sinon on se place sur le premier élément
		c.moveToFirst();
		//On créé un livre
		Pingouin georges = new Pingouin();
		//on lui affecte toutes les infos grâce aux infos contenues dans le Cursor
		georges.setId(c.getInt(NUM_COL_IDG));
		georges.setLvl(c.getInt(NUM_COL_LVLG));
        georges.setStr(c.getInt(NUM_COL_STR));
        georges.setIntel(c.getInt(NUM_COL_INTEL));
        georges.setAgi(c.getInt(NUM_COL_AGI));
        georges.setHp(c.getInt(NUM_COL_HPG));
        georges.setHpMax(c.getInt(NUM_COL_HPMAX));
        georges.setXp(c.getInt(NUM_COL_XPG));
        georges.setXpMax(c.getInt(NUM_COL_XPMAX));
        georges.setGold(c.getInt(NUM_COL_GOLDG));
		//On ferme le cursor
		c.close();

		//On retourne le livre
		return georges;
	}

	public long insertArme(Arme arme){
		//Création d'un ContentValues (fonctionne comme une HashMap)
		ContentValues values = new ContentValues();
		//on lui ajoute une valeur associé à une clé (qui est le nom de la colonne dans laquelle on veut mettre la valeur)
    values.put(COL_IDA, arme.getId());
		values.put(COL_NOM, arme.getNom());
		values.put(COL_DESC, arme.getDesc());
		values.put(COL_TYPE, arme.getType());
		values.put(COL_DMGMINA, arme.getDmgMin());
		values.put(COL_DMGMAXA, arme.getDmgMax());
		values.put(COL_PROBCRIT, arme.getProbCrit());
		values.put(COL_AUGCRIT, arme.getAugCrit());
		//on insère l'objet dans la BDD via le ContentValues
		return bdd.insert(TABLE_ARMES, null, values);
	}

	public int updateArme(int id, Arme arme){
		//La mise à jour d'un livre dans la BDD fonctionne plus ou moins comme une insertion
		//il faut simple préciser quelle livre on doit mettre à jour grâce à l'ID
		ContentValues values = new ContentValues();
		values.put(COL_NOM, arme.getNom());
		values.put(COL_DESC, arme.getDesc());
		values.put(COL_TYPE, arme.getType());
		values.put(COL_DMGMINA, arme.getDmgMin());
		values.put(COL_DMGMAXA, arme.getDmgMax());
		values.put(COL_PROBCRIT, arme.getProbCrit());
		values.put(COL_AUGCRIT, arme.getAugCrit());
		return bdd.update(TABLE_ARMES, values, COL_IDA + " = " +id, null);
	}

	public int removeArmeWithId(int id){
		//Suppression d'un livre de la BDD grâce à l'ID
		return bdd.delete(TABLE_ARMES, COL_IDA + " = " +id, null);
	}

	public Arme getArmeWithId(int id){
		//Récupère dans un Cursor les valeur correspondant à un livre contenu dans la BDD (ici on sélectionne le livre grâce à son titre)
		Cursor c = bdd.query(TABLE_ARMES, new String[] {COL_IDA, COL_NOM, COL_DESC, COL_TYPE, COL_DMGMINA, COL_DMGMAXA, COL_PROBCRIT, COL_AUGCRIT}, null, null, null, null, null);
		return cursorToArme(c);
	}

	//Cette méthode permet de convertir un cursor en un livre
	private Arme cursorToArme(Cursor c){
		//si aucun élément n'a été retourné dans la requête, on renvoie null
		if (c.getCount() == 0)
			return null;

		//Sinon on se place sur le premier élément
		c.moveToFirst();
		//On créé un livre
		Arme arme = new Arme();
		//on lui affecte toutes les infos grâce aux infos contenues dans le Cursor
		arme.setId(c.getInt(NUM_COL_IDA));
		arme.setNom(c.getString(NUM_COL_NOM));
		arme.setDesc(c.getString(NUM_COL_DESC));
		arme.setType(c.getString(NUM_COL_TYPE));
		arme.setDmgMin(c.getInt(NUM_COL_DMGMINA));
		arme.setDmgMax(c.getInt(NUM_COL_DMGMAXA));
		arme.setProbCrit(c.getInt(NUM_COL_PROBCRIT));
		arme.setAugCrit(c.getInt(NUM_COL_AUGCRIT));
		//On ferme le cursor
		c.close();

		//On retourne l'arme
		return arme;
	}

	public long insertMonstre(Monstre monstre){
		//Création d'un ContentValues (fonctionne comme une HashMap)
		ContentValues values = new ContentValues();
		//on lui ajoute une valeur associé à une clé (qui est le nom de la colonne dans laquelle on veut mettre la valeur)
	  values.put(COL_IDM, monstre.getId());
		values.put(COL_IDZONE, monstre.getIdZone());
		values.put(COL_LVLM, monstre.getLvl());
		values.put(COL_HPM, monstre.getHp());
		values.put(COL_DMGMINM, monstre.getDmgMin());
		values.put(COL_DMGMAXM, monstre.getDmgMax());
		values.put(COL_XPM, monstre.getXp());
		values.put(COL_GOLDM, monstre.getGold());
		//on insère l'objet dans la BDD via le ContentValues
		return bdd.insert(TABLE_MONSTRES, null, values);
	}

	public int updateMonstre(int id, Monstre monstre){
		//La mise à jour d'un livre dans la BDD fonctionne plus ou moins comme une insertion
		//il faut simple préciser quelle livre on doit mettre à jour grâce à l'ID
		ContentValues values = new ContentValues();
		values.put(COL_IDZONE, monstre.getIdZone());
		values.put(COL_LVLM, monstre.getLvl());
		values.put(COL_HPM, monstre.getHp());
		values.put(COL_DMGMINM, monstre.getDmgMin());
		values.put(COL_DMGMAXM, monstre.getDmgMax());
		values.put(COL_XPM, monstre.getXp());
		values.put(COL_GOLDM, monstre.getGold());
		return bdd.update(TABLE_MONSTRES, values, COL_IDM + " = " +id, null);
	}

	public int removeMonstreWithId(int id){
		//Suppression d'un livre de la BDD grâce à l'ID
		return bdd.delete(TABLE_MONSTRES, COL_IDM + " = " +id, null);
	}

	public Monstre getMonstreWithId(int id){
		//Récupère dans un Cursor les valeur correspondant à un livre contenu dans la BDD (ici on sélectionne le livre grâce à son titre)
		Cursor c = bdd.query(TABLE_MONSTRES, new String[] {COL_IDM, COL_IDZONE, COL_LVLM, COL_HPM, COL_DMGMINM, COL_DMGMAXM, COL_XPM, COL_GOLDM}, null, null, null, null, null);
		return cursorToMonstre(c);
	}

	//Cette méthode permet de convertir un cursor en un livre
	private Monstre cursorToMonstre(Cursor c){
		//si aucun élément n'a été retourné dans la requête, on renvoie null
		if (c.getCount() == 0)
			return null;

		//Sinon on se place sur le premier élément
		c.moveToFirst();
		//On créé un livre
		Monstre monstre = new Monstre();
		//on lui affecte toutes les infos grâce aux infos contenues dans le Cursor
		monstre.setId(c.getInt(NUM_COL_IDM));
		monstre.setIdZone(c.getInt(NUM_COL_IDZONE));
		monstre.setLvl(c.getInt(NUM_COL_LVLM));
		monstre.setHp(c.getInt(NUM_COL_HPM));
		monstre.setDmgMin(c.getInt(NUM_COL_DMGMINM));
		monstre.setDmgMax(c.getInt(NUM_COL_DMGMAXM));
		monstre.setXp(c.getInt(NUM_COL_XPM));
		monstre.setGold(c.getInt(NUM_COL_GOLDM));
		//On ferme le cursor
		c.close();

		//On retourne le livre
		return monstre;
	}

}
