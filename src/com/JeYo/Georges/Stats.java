package com.JeYo.Georges;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

public class Stats extends Activity {
    GeorgesBDD geo = Georges.geoBDD;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mainstats);

        geo.open();
        Pingouin g = geo.getPingouinWithId(1);
        geo.close();

        // Récupération des boutons
        final TextView lvl = (TextView) findViewById(R.id.lvl);
        lvl.setText("                    Level "+g.getLvl());
        final TextView exp = (TextView) findViewById(R.id.exp);
        exp.setText("                    Experience : "+g.getXp()+" / "+g.getXpMax());
        final TextView hp = (TextView) findViewById(R.id.hp);
        hp.setText("                    HP : "+g.getHp()+" / "+g.getHpMax());
        final TextView str = (TextView) findViewById(R.id.str);
        str.setText("                    Strength : " + g.getStr());
        final TextView intel = (TextView) findViewById(R.id.intel);
        intel.setText("                    Intelligence : " + g.getIntel());
        final TextView agi = (TextView) findViewById(R.id.agi);
        agi.setText("                    Agility : " + g.getAgi());
    }

}
