//import math.util;
package com.JeYo.Georges;

public class Arme{
  private int id;
  private String nom;
  private String description;
  private String type;
  private int dmgMin;
  private int dmgMax;
  private int probCrit;
  private int augCrit;

  public Arme(){}

  public Arme(String n, String d, String t, int dMin, int dMax,int pC, int aC) {
    type = t; nom = n; description = d; probCrit = pC; augCrit = aC; dmgMin = dMin; dmgMax = dMax;
  }

  public int calDmg(Pingouin g){
    int dmgs=(int)(Math.random()*(dmgMax-dmgMin))+dmgMin;

    if (type == "Canne") {
      if ((int)(Math.random()*100)<probCrit){
        return dmgs+augCrit;
      }
    }
    else if (type == "Chapeau") {
      if ((int)(Math.random()*100)<probCrit){
        return dmgs+augCrit;
      }
    }
    else {
      if ((int)(Math.random()*100)<probCrit){
        return dmgs+augCrit;
      }
    }
    return dmgs;
  }

  public String getDmg(){
    if (type == "Canne") {
      return dmgMin+" à "+dmgMax;
    }
    else if (type == "Chapeau") {
      return dmgMin+"";
    }
    else {
      return dmgMin+" à "+dmgMax;
    }
  }
///////////////////////////////// GETTERS ET SETTERS
  public int getId() { return id; }
  public void setId(int i) { id = i; }
  public String getNom(){ return nom; }
  public void setNom(String n){ nom = n; }
  public String getDesc(){ return description; }
  public void setDesc(String d){ description = d; }
  public String getType(){ return type; }
  public void setType(String t){ type = t; }
  public int getDmgMin() { return dmgMin; }
  public void setDmgMin(int i) { dmgMin = i; }
  public int getDmgMax() { return dmgMax; }
  public void setDmgMax(int i) { dmgMax = i; }
  public int getProbCrit() { return probCrit; }
  public void setProbCrit(int i) { probCrit = i; }
  public int getAugCrit() { return augCrit; }
  public void setAugCrit(int i) { augCrit = i; }
}
