package com.JeYo.Georges;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class MaBaseSQLite extends SQLiteOpenHelper {
/////////////////////////////////////////////////////////////// GEORGES
	private static final String TABLE_PINGOUIN = "pingouin";
	private static final String COL_IDG = "ID";
	private static final String COL_LVLG = "Lvl";
  private static final String COL_STR = "Str";
  private static final String COL_INTEL = "Intel";
  private static final String COL_AGI = "Agi";
  private static final String COL_HPG = "HP";
  private static final String COL_HPMAX = "HPMax";
  private static final String COL_XPG = "XP";
  private static final String COL_XPMAX = "XPMax";
  private static final String COL_GOLDG = "Gold";

	private static final String CREATE_BDD_PINGOUIN = "CREATE TABLE " + TABLE_PINGOUIN + " ("
	+ COL_IDG + " INTEGER PRIMARY KEY , " + COL_LVLG + " INTEGER NOT NULL, "
	+ COL_STR + " INTEGER NOT NULL, " + COL_INTEL + " INTEGER NOT NULL, " + COL_AGI  + " INTEGER NOT NULL, "
  + COL_HPG  + " INTEGER NOT NULL, " + COL_HPMAX  + " INTEGER NOT NULL, " + COL_XPG  + " INTEGER NOT NULL, "
  + COL_XPMAX  + " INTEGER NOT NULL, "  + COL_GOLDG + " INTEGER NOT NULL);";

/////////////////////////////////////////////////////////////// ARMES
  private static final String TABLE_ARMES = "armes";
	private static final String COL_IDA = "ID";
	private static final String COL_NOM = "Nom";
  private static final String COL_DESC = "Description";
  private static final String COL_TYPE = "Type";
  private static final String COL_DMGMINA = "DmgMin";
  private static final String COL_DMGMAXA = "DmgMax";
  private static final String COL_PROBCRIT = "ProbCrit";
  private static final String COL_AUGCRIT = "AugCrit";

  private static final String CREATE_BDD_ARMES = "CREATE TABLE " + TABLE_ARMES + " ("
	+ COL_IDA + " INTEGER PRIMARY KEY AUTOINCREMENT, " + COL_NOM + " TEXT NOT NULL, "
	+ COL_DESC + " TEXT NOT NULL, " + COL_TYPE + " TEXT NOT NULL, " + COL_DMGMINA  + " INTEGER NOT NULL, "
  + COL_DMGMAXA  + " INTEGER NOT NULL, " + COL_PROBCRIT  + " INTEGER NOT NULL, "+ COL_AUGCRIT + "INTEGER NOT NULL);";

/////////////////////////////////////////////////////////////// MONSTRES
	private static final String TABLE_MONSTRES = "monstres";
	private static final String COL_IDM = "ID";
	private static final String COL_IDZONE = "IDZ";
	private static final String COL_LVLM = "Lvl";
  private static final String COL_HPM = "HP";
  private static final String COL_DMGMINM = "DmgMin";
  private static final String COL_DMGMAXM = "DmgMax";
  private static final String COL_XPM = "XP";
  private static final String COL_GOLDM = "Gold";

	private static final String CREATE_BDD_MONSTRES = "CREATE TABLE " + TABLE_MONSTRES + " ("
	+ COL_IDM + " INTEGER PRIMARY KEY AUTOINCREMENT, " + COL_IDZONE + " INTEGER NOT NULL, "+ COL_LVLM + " INTEGER NOT NULL, "
	+ COL_DMGMINM  + " INTEGER NOT NULL, " + COL_DMGMAXM  + " INTEGER NOT NULL, "
	+ COL_HPM  + " INTEGER NOT NULL, " + COL_XPM  + " INTEGER NOT NULL, "
  + COL_GOLDM + " INTEGER NOT NULL);";

/////////////////////////////////////////////////////////////// ZONES
	private static final String TABLE_ZONES = "zones";
	private static final String COL_IDZ = "ID";
	private static final String COL_LVLZ = "Lvl";

	private static final String CREATE_BDD_ZONES = "CREATE TABLE " + TABLE_ZONES + " ("
	+ COL_IDZ + " INTEGER PRIMARY KEY AUTOINCREMENT, " + COL_LVLZ + " INTEGER NOT NULL);";


	public MaBaseSQLite(Context context, String name, CursorFactory factory, int version) {
		super(context, name, factory, version);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		//on créé la table à partir de la requête écrite dans la variable CREATE_BDD
		db.execSQL(CREATE_BDD_PINGOUIN);
		db.execSQL(CREATE_BDD_ARMES);
		db.execSQL(CREATE_BDD_MONSTRES);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		//On peut fait ce qu'on veut ici moi j'ai décidé de supprimer la table et de la recréer
		//comme ça lorsque je change la version les id repartent de 0
		db.execSQL("DROP TABLE " + TABLE_PINGOUIN + ";");
		onCreate(db);
	}

}
