package com.JeYo.Georges;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

/**
 * Created by rouzaud on 01/04/16.
 */
public class Travel extends Activity {
    GeorgesBDD geo = Georges.geoBDD;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.maintravel);

        geo.open();
        Pingouin g = geo.getPingouinWithId(1);
        Monstre m = geo.getMonstreWithId(1);
        geo.close();
        int hmm = m.getHp();

        // Récupération des champs Georges
        final TextView lvlGeo = (TextView) findViewById(R.id.lvlGeo);
        lvlGeo.setText("Level "+g.getLvl());
        final TextView hpGeo = (TextView) findViewById(R.id.hpGeo);
        hpGeo.setText("HP : "+g.getHp()+" / "+g.getHpMax());

        // Récupération des champs monstres
        final TextView lvlMob = (TextView) findViewById(R.id.lvlMob);
        lvlMob.setText("Level "+m.getLvl());
        final TextView hpMob = (TextView) findViewById(R.id.hpMob);
        hpMob.setText("HP : "+m.getHp()+" / "+hmm);
    }
}